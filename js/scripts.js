(function() {

var links = new Links();
links.addHeader("Sample Header");
links.addHeader("Sample Header2");
links.addHeader("Sample Header3");
links.addHeader("Sample Header4");

links.addLink("Sample Header", 0, "facebook","facebook.com");
links.addLink("Sample Header2", 0, "samplelink1","expressen.se");
links.addLink("Sample Header3", 0, "Samplelink2","dn.se");
links.addLink("Sample Header4", 0, "Samplelink", "sd.sd");
links.addLink("Sample Header4", 1, "Samplelink3", "sd.sd");
links.addLink("Sample Header4", 2, "Samplelink3", "sd.sd");
links.addLink("Sample Header4", 3, "Samplelink3", "sd.sd");
links.addLink("Sample Header4", 4, "Samplelink3", "sd.sd");
links.setColorOfHeader("Sample Header2", "blue");

console.log(links.getLinks());

var le = new LinkEditor({"c":0, "s":1,"a":0});
le.setKeyCode(87);
le.setConstraints({"c":0, "s":1,"a":0});

var app = angular.module('app', []);
app.controller("EditorModeController", function($scope) {
	$scope.editorMode=false;
	$scope.keyDown=false;
	$scope.changeMode=function() {
		$scope.editorMode=!$scope.editorMode;
		console.log("changing mode");
	}

	$scope.toggleMode = function() {
		$scope.changeMode();
		le.handleClick();
	};

});

app.controller('LinkController', function($scope)
{
	$scope.allLinks = links.getLinks();
	$scope.showLinkInputBoxes = false;

	$scope.toggleInput = function() {
		$scope.showLinkInputBoxes=!$scope.showLinkInputBoxes;
	}
	$scope.toggleInputHeaderEdit = function() {
		console.log("method call");
		$scope.showHeaderInputBox=!$scope.showHeaderInputBox;
	}
	$scope.showText = function(a) {
		return a == true ? 'Save' : 'Edit';
	}
	$scope.deleteLink = function(headerPos,linkPos) {
		//splice array
		$scope.allLinks[headerPos].keys.splice(linkPos,1);
	}

	$scope.deleteNode = function(headerPos) {
		$scope.allLinks.splice(headerPos,1);
	}

	$scope.addLink = function(headerPos) {
		$scope.allLinks[headerPos].keys.push({"label":"sampel label","URL":"www.example.com"});
	}

	$scope.addNode = function() {
    links.addHeader("New header");
	}

  $scope.changeColor = function(headerPos) {
    $scope.allLinks[headerPos].backgroundcolor=ColorScheme.getNextColor($scope.allLinks[headerPos].backgroundcolor);
  }

});
app.controller('ToggleEditorModeController', function($scope) {
	$scope.showViewEditText = function(editorMode) {
		return editorMode == true ? "Normal mode" : "Edit mode";
	}
});
/*
	Weather
 */
var weather = new Weather();
weather.loadWeatherPlacesFromLocalStorage();
weather.addPlaceAndCoords("Karlskrona", [55, 12]);
weather.addPlaceAndCoords("Hässelby", [12,13]);
app.controller('WeatherController', function($scope)
{
	//add labels for names
	$scope.weatherCitiesLabels = weather.getPlaces();
});


/*

	Set width of .slide to be as x times large as the amount of .sliders, whereas x is the screen size. 

*/
var slider = new Slider();
slider.setPages(2);
slider.setCurrentPage(0);
var w = window.innerWidth;
var elems = document.getElementsByClassName("slide");
var count = elems.length;
var eom = 200;

var calc = (w*count)+eom;
var main = document.getElementById('slider');
main.style.width=calc+"px";

//We set an error of margin because CSS is retarded.
for(var i = 0; i < count; i++)
{
	elems[i].style.width=window.innerWidth+"px";
}

var slideLeft = document.getElementById('slideLeft');
slideLeft.addEventListener("click", function(){	
	console.log("trying to move " + window.innerWidth);
	slider.slideLeft(window.innerWidth);
});

var slideRight = document.getElementById('slideRight');
slideRight.addEventListener("click", function(){
	console.log("trying to move " + window.innerWidth);
	slider.slideRight(window.innerWidth);
});

window.addEventListener("resize", function(){
	slider.readjustWidth(window.innerWidth);
});

links.saveToLocalStorage();


//
// document.addEventListener("keydown", function(e) {
//    console.log(e.keyCode);
//	if(e.keyCode == 87) { //w 
//		le.handleKeyPress(e.keyCode);
//	}
//	if(e.keyCode == 16)	{//shift
//		le.setShift(1);
//	}
//	else if(e.keyCode == 17) {//ctrl
//		le.setCtrl(1);
//	}
//	else if(e.keyCode == 18) {//alt
//		le.setAlt(1);
//	}
// });
//document.addEventListener("keyup", function(e) {
//	if(e.keyCode == 16)
//		le.setShift(0);
//	else if(e.keyCode == 17)
//		le.setCtrl(0);
//	else if(e.keyCode == 18)
//		le.setAlt(0);
//});
//
//TODO: DRAG AND DROP VERTICALLY




})();
