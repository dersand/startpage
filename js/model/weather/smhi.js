let json = localStorage.getItem("app_weather_smhi_api_data");

/** A day has multiple time periods(01:00,02:00...)
	*
	*
	*/
class TimePeriod {
		constructor(celsius,
								windSpeed,
								thunderProb,
								minPrec,
								maxPrec,
								time) {
			// Degrees in celsius
			this.celsius=celsius

			// Wind speed in m/s
			this.windSpeed=windSpeed

			// Thunder probability (0-100)
			this.thunderProb=thunderProb

			// Minimum precipitation (mm/h)
			this.minPrec=minPrec

			// Minimum precipitation (mm/h)
			this.maxPrec=maxPrec

			// Set HH:mm time
			this.time=time
		}
}

class Day {
	constructor(timestamp) {
		this.timestamp=new Date(timestamp)
		this.day=this.timestamp.getDay()

		this.points=new Array()
	}

	toString() {
		let obj = this.computeAverages()
		return {"day": this.day,
						"avg": [
						{
						"degree":obj[0],
						"unit": "c"
						},
						{
						"degree":obj[1],
						"unit": "m/s"
						},
						{
						"degree":obj[2],
						"unit": "%"
						},
						{
						"degree":obj[3],
						"unit": "mm/h"
						},
						{
						"degree":obj[4],
						"unit": "mm/h"
						}
						]
					}
	}
	computeAverages() {
		let arr=[0,0,0,0,0]
		let c = null

		for ( let i=0; i<this.points.length; i++) {
			c = this.points[i]
			arr[0]+=c.celsius
			arr[1]+=c.windSpeed
			arr[2]+=c.thunderProb
			arr[3]+=c.minPrec
			arr[4]+=c.maxPrec
		}
		// Re use c
		c = this.points.length

		// Average = total-value-of-all-elements/count-of-all-elements
		// Round to two decimals
		for( let j=0; j<c; j++) {
			arr[j]=Math.round((arr[j]/c) * 100 / 100)
		}

		return arr
	}

	addTimePeriod(celsius,
								windSpeed,
								thunderProb,
								minPrec,
								maxPrec,
								time) {
		this.points.push(new TimePeriod(celsius,windSpeed,thunderProb,minPrec,maxPrec,time))
	}
}

class SMHI {
	constructor(iJson) {
		this.json=JSON.parse(iJson)
		this.days=new Array(new Day(this.json.timeSeries[0].validTime))
		this.generateDays()
	}

	intHourToString(day) {
		// Say no to sunday being start of week
		let normalizedDay = (day==0) ? 6 : day-1
		let k=["Monday", "Tuesday", "Wednesday","Thursday", "Friday", "Saturday", "Sunday"]
		return k[normalizedDay]

	}

	getAverageOfDay(day) {
		return this.days[day].computeAverages()
	}

	getObjectOfDay(day) {
		return this.days[day].toString()
	}

	generateDays() {
		let maximum_days = 3 // Don't cache more than 4 days
		let series = this.json.timeSeries

		let timestamp=new Date(series[0].validTime)
		let tempTimestamp=null
		let dayString = timestamp.getDay()
		let currentDay=0

		for(let i=0; i<series.length; i++) {
			let section=series[i]

			let tempTimestamp=new Date(section.validTime)
			let params = section.parameters

			if(tempTimestamp.getDay() != dayString) {
				dayString=tempTimestamp.getDay()
				this.days.push(new Day(tempTimestamp))
				currentDay++;
			}

			let v=[this.getCelsius(params),
						this.getWindSpeed(params),
						this.getThunderProb(params),
						this.getMinPrec(params),
						this.getMaxPrec(params),
						this.getTime(section)]
			this.days[currentDay].addTimePeriod(v[0],v[1],v[2],v[3],v[4],v[5]);
		}

		// Remove maximum-days to this.days.length
		let newArr=[]
		for(let i=0; i<maximum_days; i++) {
			newArr.push(this.days[i])
		}
		this.days=newArr
		console.info("Days: %o", this.days)
	}

	getCelsius(param) { return param[1].values[0] }
	getWindSpeed(param) { return param[4].values[0] }
	getThunderProb(param) { return param[6].values[0] }
	getMinPrec(param) { return param[12].values[0] }
	getMaxPrec(param) { return param[12].values[0] }
	getTime(series) {
		let d = new Date(series.validTime)
		let h=d.getHours()
		h=(parseInt(h)<10) ? "0"+h : h // one liner, add 0 if e.g. 9(09:00)>(9:00)
		return h+":00"
	}
}
