/*
  functions
*/
function isLocalStorageSupported()
{
  var res = false;
  if(typeof Storage !== "undefined")
  {
    res = true;
  }
  return res;
}

function LocalStorageKeys()
{
  this.keys = {
    "weatherPlacesAndCoords":"app_weather_places_and_coords",
    "linkWebpagelinks" : "app_link_web_page_links"
  };
}

function toLocalStorage(key, data)
{
  var ret = false;

  if(isLocalStorageSupported())
  {
    ret = true;
    localStorage.setItem(key, data);
  }
}

function Weather()
{
  var placesAndCoords = [];


  //Getters
  this.getPlaces= function(){return placesAndCoords;}

  //Setters
  this.setPlaces = function(iPlaces){placesAndCoords = iPlaces;}

  //Functions
  this.getPlacesAt = function(index){return placesAndCoords[0];}

}
Weather.prototype.loadWeatherPlacesFromLocalStorage= function ()
{
  if(isLocalStorageSupported())
  {
    var weatherKey = new LocalStorageKeys().keys['weatherPlacesAndCoords'];

    var placesAndCoords = localStorage.getItem(weatherKey);

    //If there is any places and coords in the localstorage.
    if(placesAndCoords != null && typeof placesAndCoords !== undefined)
    {
      console.log("places is set");
      this.setPlaces(places);
    }
    else
    {
      console.log("places is null");
    }
  }
};

Weather.prototype.saveAllToLocalStorage = function(){
    var key = new LocalStorageKeys().keys['weatherPlaces'];
    var k = toLocalStorage(key, this.getPlaces());
};
Weather.prototype.addPlaceAndCoords = function (place, coords) {
  this.getPlaces().push({'p':place, 'c':coords});
};


Weather.prototype.deletePlaceAndCoords = function (place) {
  for(var i = 0; i < this.getPlaces().length; i++)
  {
    if(this.getCoordsAt(i)['p'] == place)
    {
      this.getCoordsAt(i)=null;
      i = getCoords.length;
    }
  }
};

function ColorScheme()
{
}

ColorScheme.RED       = "255,59,48";
ColorScheme.ORANGE    = "255,149,0";
ColorScheme.YELLOW    = "255,204,0";
ColorScheme.GREEN     = "76,217,100";
ColorScheme.TEAL_BLUE = "90,200,250";
ColorScheme.BLUE      = "0,122,255";
ColorScheme.PURPLE    = "88,86,214";
ColorScheme.PINK      = "255,45,85";
ColorScheme.colors = {
  "red":        ColorScheme.RED,
  "orange":     ColorScheme.ORANGE,
  "yellow":     ColorScheme.YELLOW,
  "green":      ColorScheme.GREEN,
  "teal_blue":  ColorScheme.TEAL_BLUE,
  "blue":       ColorScheme.BLUE,
  "purple":     ColorScheme.PURPLE,
  "pink":       ColorScheme.PINK
}

ColorScheme.rainbow = [ColorScheme.RED, ColorScheme.ORANGE, ColorScheme.YELLOW, ColorScheme.GREEN, ColorScheme.TEAL_BLUE, ColorScheme.BLUE, ColorScheme.PURPLE, ColorScheme.PINK];

ColorScheme.getNextColor = function(currentColor) {
  let color = "";
  for(var i=0;i<ColorScheme.rainbow.length;i++)
  {
    ColorScheme.rainbow[i];
    if (i+1 == ColorScheme.rainbow.length)
    {
      color = ColorScheme.rainbow[0];
      i = ColorScheme.rainbow.length;
    }
    else if (ColorScheme.rainbow[i] == currentColor)
    {
      color = ColorScheme.rainbow[i+1];
      i = ColorScheme.rainbow.length;
    }
  }
  return color;
}

ColorScheme.getRandomColor = function()
{
  var r = Math.floor((Math.random() * ColorScheme.rainbow.length) + 0);
  let element = ColorScheme.rainbow[r];
  console.log("Random: " + element + " ("+r+")");
  return element;
}


function Links()
{
	var links = [];

	//Getters
	this.getLinks = function(){return links;}

	//Setters
	this.setLinks = function(iLinks){links=iLinks;}
	
	//Functions
};

Links.prototype.addLink = function(header, pos, name, url){
	var links = this.getLinks();
	
	var headerPos = this.getPosOfHeader(header);
	if(headerPos == -1)
	{
		this.getLinks().push(this.createHeader(header));
		headerPos=links.length-1;
	}
	links[headerPos].keys[pos]={"label":name, "URL":url};

	
}
Links.prototype.addHeader = function(headerName)
{
  this.getLinks().push(this.createHeader(headerName));
}
Links.prototype.createHeader = function(headerName)
{
  var temp = {};
  temp['header'] = headerName;
  temp['keys'] = [];
  temp['backgroundcolor'] = ColorScheme.getRandomColor();
  return temp;
}
Links.prototype.getPosOfHeader = function(queriedHeader)
{
	var res = -1;
	var links = this.getLinks();
	for(var i = 0; i < links.length; i++)
	{
		if(links[i].header == queriedHeader)
		{
			res = i;
			i = links.lenth;
		}
		//console.log(links[i].header);
		
	}

	return res;
}
Links.prototype.setColorOfHeader = function(queriedHeader, color)
{
  var res = -1;
	var links = this.getLinks();
	for(var i = 0; i < links.length; i++)
	{
		if(links[i].header == queriedHeader)
		{
      links[i].backgroundcolor = ColorScheme.colors[color];
		}
		
	}
 
}

Links.prototype.linkToJson = function()
{
  var out     = "";
  var links   = this.getLinks();
  var header  = null;
  var link    = null;

  // Epilogue
  out+="[ ";

  for(var i=0;i<links.length;i++)
  {
    header = links[i];
    out+="{\"header\": \"" + header.header+ "\", \"backgroundcolor\":\"" + header.backgroundcolor + "\",\"keys\":[";
    for(var j=0;j<header.keys.length;j++)
    {
      link=header.keys[j];
      if(j+1==header.keys.length) {
        out+="{\"LABEL\": \""+link.label+"\", \"URL\":\""+link.URL+"\"}";
      }
      else
      {
        out+="{\"LABEL\": \""+link.label+"\", \"URL\":\""+link.URL+"\"},";
      }
    }
    // close keys variable
    out+="]";

    // Close header object
    out+="}";

    // If there is more objects, append with ","
    out += (i+1==links.length) ? "" : ",";
   
  }

  // Prologue
  out+="]";

  console.log(out);

}

Links.prototype.saveToLocalStorage = function()
{
  var data = this.linkToJson();
  var key = new LocalStorageKeys().keys['linkWebpagelinks'];

  var ret = toLocalStorage(key,data);
  console.log("Saved? : " +true);
  
  
}

function Slider()
{
	var pages = 0;
	var currentPage = 0;
	
	//setters
	this.setPages = function(iPages){this.pages=iPages;}
	this.setCurrentPage = function(page){this.currentPage=page;}

	//Getters
	this.getPages = function(){return this.pages;}
	this.getCurrentPage = function(){return this.currentPage;}	

}
Slider.prototype.pixelParse = function(mode, value1, value2)
{
	var i = parseInt(value1);
	var j = parseInt(value2);

	if(isNaN(i))
	{
		i = 0;
	}

	if(mode == "+")
		return i-j;
	else
		return i+j;
}
Slider.prototype.slideLeft = function(width)
{
	var pages = this.getPages();
	var currentPage = this.getCurrentPage();

	if((currentPage-1)>=0)
	{
		//Valid move, go ahead and move
		var elem = document.getElementById('slideMove');
		var nWidth = this.pixelParse("-", elem.style.marginLeft, width);
		
		elem.style.marginLeft = nWidth + "px";
		this.setCurrentPage(currentPage-1);
	}
}

Slider.prototype.slideRight = function(width)
{
	var pages = this.getPages();
	var currentPage = this.getCurrentPage();

	if((currentPage+1) < pages)
	{
		//Valid move

		var elem = document.getElementById('slideMove');
		var nWidth = this.pixelParse("+", elem.style.marginLeft, width);
		
		elem.style.marginLeft = nWidth + "px";
		this.setCurrentPage(currentPage+1);
	}
}

Slider.prototype.readjustWidth = function(width) {
	//Each slide should be new width's width.

	//Slidemove should be parseInt(elem.style.marginLeft) * currentPage * width

	var elem = document.getElementById('slideMove');
	var oldWidth = elem.style.marginLeft;
	oldWidth = parseInt(oldWidth);

	var newWidth = 0;
	if(this.getCurrentPage() == 0)
			newWidth = width;
		else 
			newWidth = this.getCurrentPage() * width;

	if(oldWidth < 0)
	{
		newWidth = -newWidth;
	}
	
	var elem2 = document.getElementsByClassName("slide")
	for(var i = 0; i < this.getPages(); i++)
	{
		elem2[i].style.width = newWidth + "px";
	}
}

var LinkEditor = function(constraints) {

	var inLinkMode=false;
	var keyCode = 87;
	var constrains = {"c":0,"a":0,"s":0};
	var critical = 0;
	var CTRL = 0;
	var SHIFT =  0;
	var ALT =  0; 

	//Getters
	this.getCtrl= function(){this.criticalSection();return CTRL;this.setCritical(0);}
	this.getAlt= function(){this.criticalSection();return ALT;this.setCritical(0);}
	this.getShift= function(){this.criticalSection();return SHIFT;this.setCritical(0);}
	this.getCritical = function(){return this.critical;}
	this.getKeyCode = function(){return keyCode;}
	this.getConstraints = function(){return constraints;}
	this.getMode= function(){return inLinkMode;}


	//Setters
	this.setCtrl= function(ctrl){this.criticalSection();CTRL = ctrl;this.setCritical(0);}
	this.setAlt= function(alt){this.criticalSection();ALT = alt;this.setCritical(0);}
	this.setShift= function(shift){this.criticalSection();SHIFT = shift;this.setCritical(0);}
	this.setCritical = function(crit){critical = crit;}
	this.setKeyCode = function(e){keyCode = e;}
	this.setConstraints = function(constr){constraints=constr;}
	this.setMode= function(mode){inLinkMode=mode;}
	

	//Functions
	this.criticalSection = function()
	{
		while(this.getCritical() == 0)
		{
			this.setCritical(1);
		}
	};

	this.matchedConstraints = function()
	{
		matched=false;
		var cConstraints = this.getConstraints();
		if(cConstraints.c == 0 && cConstraints.s == 0 && cConstraitns.a == 0)
		{
			//If there's no constrains, then there is no match.
			matched = true;
		}
		else {
			var our = {"c":this.getCtrl(),"s":this.getShift(),"a":this.getAlt()};
				var their = cConstraints;
			if ( our.c == their.c && our.s == their.s && our.a == their.a)
			{
				matched = true;
			}
		}
		return matched;
	}
}

LinkEditor.prototype.toggleEditorMode = function() {
	if(this.getMode()==false) {
		this.setMode(true);
		return true;
	}
	else if(this.getMode() == true) {	
		this.setMode(false);
		return false;
	}
}

LinkEditor.prototype.enterEditorMode = function() {
		console.log("Entering editor mode");

	var elems=document.getElementsByClassName("linkPanel"), elem, html, headerValue, subChild, linkHeader, links, link, linkText, linkUrl, headerEdit, nodeDelete, htmlText,htmlUrl,linkEdit,linkDelete;
	for(var i=0;i<elems.length;i++) {
		//Element setters
		//Set element[i]
		elem = elems[i];

		// elem.style.outline="2px solid green";

	}
}

LinkEditor.prototype.enterViewMode= function() {
	console.log("Entering view mode");
	var elems=document.getElementsByClassName("linkPanel"), elem, html, headerValue, subChild, linkHeader, linkText, linkUrl, headerEdit, nodeDelete, deleteElem, linkEdit, linkDelete;

	for(var i=0;i<elems.length;i++) {
		//Element setters
		//Set element[i]
		elem = elems[i];

		// elem.style.outline="none";	
  }
}

LinkEditor.prototype.handleKeyPress = function(ikeycode) {

	if(ikeycode == this.getKeyCode() && this.matchedConstraints()) {
		if(this.toggleEditorMode()) {
			this.enterEditorMode();
		} else {
			this.enterViewMode();
		}
		
	}
}

LinkEditor.prototype.handleClick= function() {
	var k = this.toggleEditorMode();
	if(k) {
		this.enterEditorMode();
	} else {
		this.enterViewMode();
	}
	return k;
}
